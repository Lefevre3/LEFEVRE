28/01/2020    
Alex Lefevre     
    
# Etape 0 :  
   
![sources.list](Sourceslist.png)    
     
     
cd /etc/apt -> cat sources.list / vim sources.list    
[Debian GNU/Linux 10.1.0 Buster - Official i386 NETINST 20190908-02:37]    
sudo apt update   
sudo apt dist-upgrade   
Il faut mettre à jour la liste des logiciels présents sur la machine pour tous les mettre à jour, sinon certains risquent de ne pas être mis à jour.   
   
# Etape 1 :   
dpkg --status apache2   
sudo apt install apache2   
lancer : sudo systemctl start apache2    
arrêter : sudo systemctl stop apache2   
relancer : sudo systemctl restart apache2   
   
# Etape 2 :   
   
getent group www-data / cat /etc/group | grep www-data   
ls -ld /var/www    
sudo chgrp -R www-data /var/www   
Group Owner et seulement exécuter et lire(r-x) - Commande : ls -ld /var/www    
   
# Etape 3 :
    
sudo adduser lucky   PUIS  sudo adduser lucky www-data    
lucky peut lire et exécuter les fichiers de /var/www. Il ne peut donc pas ajouter un nouveau fichier dans /var/www   
sudo chmod 775 -R /var/www    
touch /var/www/html/moi.html   
(su - lucky)   
echo Je suis Alex > /var/www/html/moi.html    
     
# Etape 4 :    
    
sudo mkdir -p /var/www/monsite.com/public_html   
sudo chown -R $USER:$USER /var/www/monsite.com/public_html    
sudo chmod -R 755 /var/www   
sudo vim /var/www/monsite.com/public_html/index.html
créer le dossier dans home/lucky
a2enmod userdir
modifier le document root dans le fichier conf de apache2/sites-available
     
# Etape 5 :     
      
sudo pstree    
ps aux --forest    
pidof apache2    
kill -numerooption PID(numéro du proc)    
ps -e | grep apache2    


