12/03/2020    
Alex Lefevre     
    quelles raisons de fixer son adresse publique
# Etape 1 :    
   
Wireshark permet de détecter les échanges entre client et serveur     
    
On peut capturer les échanges et voir l'activité sur son réseau ainsi que le nombre de requêtes(statistiques)    
![enableping](images/enableping.png)  
    
# Etape 2 :  
    
1) Il permet de communiquer entre un client et un serveur. Il appartient à la couche 7 - Application. Les navigateurs web. Wamp par exemple.     
     
2) Une ligne de requête puis les champs d'en-tête de la requête puis le corps de la requête. Réponse : une ligne de statut puis les champs d'en-tête de la réponse puis le corps de la réponse.   
      
3) Mozilla Firefox, sur windows 10 famille, avec 192.168.1.16 et ma machine hôte.     
     
4) Apache, sur machine hôte, windows 10 famille et 192.168.1.16 avec le port 80    
     
5) Le fichier captureetape2.pcapng avec filtre http    
      
6) Wireshark ne parvient pas à capter le trafic en local (Assuming that your client and server are on the same machine and your OS is Windows (as you're using VB), then Wireshark, or more precisely WinPCap, can't easily capture such traffic). J'ai capturé une requête http allant de mon adresse à celle du site la-valla-en-gier.fr    
![captureq6](images/captureq6.png)
     
# Etape 3 :    
    
1) Il sert à partager des fichiers. Il appartient aussi à la couche 7 - Application. WinSCP fait des requetes FTP. FileZilla Server répond.       
2) Le client envoie des commandes au serveur FTP, des chaines de caractères Telnet terminées par le code de fin de ligne Telnet. La réponse est composée de 3 chiffres indiquant le statut de la requête(réussi ou non) avec une série de caractères descriptifs.   
     
3) WinSCP sur la machine hôte, windows 10 famille, 192.168.1.16    
   
4) Machine distante, OS Unix, sur le port 21 et l'ip 35.209.241.59    
![ftpq4](images/ftpq4.png)    
    
5) Le fichier ftpq5.pcapng avec filtre ftp    
    
6) ![ftpq6](images/ftpq6.png)   
     
# Etape 4 :    
    
1) Il sert à ouvrir une session interactive afin d'envoyer des commandes ou des fichiers chiffrés. Il appartient aussi à la couche 7. Une application est OpenSSH.    
      
2) Le client et le serveur s'identifie pour mettre en place un canal sécurisé. Le client s'authentifie ensuite auprès du serveur pour avoir une session.    
    
3) PuTTY sur la machine hôte, windows 10 famille, 192.168.1.16     
     
4) OpenSSH, Machine virtuelle, Debian, 10.0.2.15(127.0.1.1), port 22    
![ftpq4](images/puttyconnect.png)     
    
5) Le fichier ssh.pcapng avec filtre tcp.   
      
6) Wireshark ne parvient pas à détecter les échanges en local.
