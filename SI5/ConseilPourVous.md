25/06/2020    
Alex Lefevre     
    
J'aurais préféré plus de correction d'activité, pour nous montrer la manière optimale d'arriver au résultat. Même lorsque l'on réussit, ce n'est pas toujours de la bonne manière et presque jamais la meilleure. Sinon le reste était très bien.