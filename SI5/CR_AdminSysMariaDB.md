07/05/2020    
Alex Lefevre     
    
# Etape 1 :   
     
sudo apt install mariadb-server    
sudo mysql -u root (vérifie l'installation)    
root n'a pas de mot de passe    
SET PASSWORD FOR 'root'@'localhost' = PASSWORD('mdpchoisi');   
systemctl start mariadb.service     
systemctl status mariadb.service     
systemctl stop mariadb.service      
systemctl restart mariadb.service      
sudo mysql -u root     
CREATE DATABASE db1; (query ok...)     
/etc/mysql/my.cnf et /etc/alternatives/my.cnf (/etc/mysql/my.cnf.fallback)        
    
# Etape 2 :   
      
sudo apt update   
sudo apt install -y curl wget gnupg2 ca-certificates lsb-release apt-transport-https    
wget https://packages.sury.org/php/apt.gpg    
sudo apt-key add apt.gpg    
sudo echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php7.list   
sudo apt update    
sudo apt install -y php7.3 php7.3-cli php7.3-common   
sudo update-alternatives --set php /usr/bin/php7.3   
php -v   
sudo apt install -y apache2 libapache2-mod-php7.3   
     
# Etape 3 :   
    
sudo mkdir /var/www/html/adminer    
sudo wget "http://www.adminer.org/latest.php" -O /var/www/html/adminer/latest.php   
sudo ln -s /var/www/html/adminer/latest.php /var/www/html/adminer/adminer.php    
echo "Alias /adminer.php /var/www/html/adminer/adminer.php" | sudo tee /etc/apache2/conf-available/adminer.conf    
sudo a2enconf adminer.conf    
sudo systemctl reload apache2    
Firefox et Debian 10    
     
# Etape 4 :   
    
Pour que les deux machines virtuelles puissent communiquer, il faut régler le mode d'accès réseau sur virtualbox sur "Réseau interne", puis il faut attribuer une adresse IPv4 (192.168.0.1 et 192.168.0.2 par exemple) différente à chaque machine virtuelle. La connexion se fait de la manière suivante : Machine virtuelle 1 (Adminer) <-> Switch virtuelle <-> Machine virtuelle 2 (MariaDB)      
ping 192.168.0.1 sur la machine 2 et ping 192.168.0.2 sur la machine 1    
J'ai du relancer mariaDB : service mariadb restart / mysql -u root -p / je n'arrivais pas à me connecter car il faut forcément un mot de passe avec adminer / j'ai aussi du relancé une des machines    
     
# Etape 5 :      
      
![interne.list](images/interne.png)   