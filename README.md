# Alex LEFEVRE  
26/08/19  
Mail: <alex.lefevre99@orange.fr>

## Où j'habite :  
*Jouy Le Moutier*

## Comment je viens à Itescia :   
*Voiture (20mn)*

## Derniers diplômes :   
*Bac L(CNED Versailles,2017),L1 Lettres Modernes (Université Cergy Pontoise,2018)*

## Pourquoi BTS SIO :   
*Je souhaitais me réorienter vers un domaine proche des mathématiques tout en gardant le côté créatif des lettres.*

## Qu'est ce que vous savez en informatique :   
*Bases en html, css.*

## Mes forces :   
* Patience 
* Perséverance.

## Mes faiblesses :   
*Moins concentré le matin.*

## Comment je travaille :   
*Je travaille surtout le week-end et assez peu le soir après les cours.*