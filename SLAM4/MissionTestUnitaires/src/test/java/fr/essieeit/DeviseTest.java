package fr.essieeit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DeviseTest {
    Devise m12CHF= new Devise(12, "CHF");
    Devise m14CHF= new Devise(14, "CHF");
    Devise m14USD = new Devise(14, "USD");
    static int compteur=0;

    @Test
    public void testAdd() throws MonnaieDifferenteException {
        Devise result = m12CHF.add(m14CHF);
        Devise expected = new Devise(26, "CHF");
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testTestEquals() {
        Assert.assertEquals(m12CHF.equals(m12CHF),true);
        Assert.assertEquals(m12CHF.equals(m14CHF),false);
        Assert.assertEquals(m14CHF.equals(m14USD),false);
    }

    @Test(expected = MonnaieDifferenteException.class)
    public void testDeviseFormat() throws MonnaieDifferenteException {
        m12CHF.add(m14USD);
    }

    @Before
    public void methodeBefore() {
        compteur++;
        System.out.println(compteur+"e passage avant exécution d'une méthode de test");
    }
    @After
    public void methodeAfter() { System.out.println(compteur+"e passage APRES exécution d'une méthode de test");  }
}