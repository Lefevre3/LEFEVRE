package fr.essieeit;

import junit.framework.TestCase;
import org.junit.Assert;

public class PortefeuilleTest extends TestCase {
    Portefeuille portefeuille = new Portefeuille();
    Portefeuille portefeuille2 = new Portefeuille();

    public void testAjouteDevise() {
        portefeuille.ajouteDevise(new Devise(5,"EUR"));
        portefeuille.ajouteDevise(new Devise(5,"EUR"));
        portefeuille.ajouteDevise(new Devise(5,"CHF"));
        portefeuille.ajouteDevise(new Devise(10,"CHF"));

        portefeuille2.ajouteDevise(new Devise(10,"EUR"));

        Assert.assertEquals(portefeuille.getContenu().get("EUR"),portefeuille2.getContenu().get("EUR"));
        Assert.assertNotEquals(portefeuille.getContenu().get("CHF"),portefeuille2.getContenu().get("CHF"));
    }
}