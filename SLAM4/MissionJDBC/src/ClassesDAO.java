import java.util.ArrayList;

public class ClassesDAO {
    public ArrayList<Classes> recupererALL(Connexion maConnexion){
        String req = "SELECT * FROM classes";
        ArrayList<Classes> tableClasses = maConnexion.requeteSelect(req);
        return tableClasses;
    }

    public Classes recupererMembreByIdcl(String idcl, Connexion maConnexion){
        String req1 = "SELECT * FROM classes WHERE idcl="+idcl;
        ArrayList<Classes> tableClasses = maConnexion.requeteSelect(req1);
        Classes response = tableClasses.get(0);
        return response;
    }

    public void ajouterClasses(Classes classes, Connexion maConnexion){
        String req = "INSERT INTO classes VALUES ("+classes.getIdcl()+", '"+classes.getLibellecl()+"')";
        maConnexion.requeteUpdate(req);
    }

    public void supprimerClasses(String idcl, Connexion maConnexion){
        String req = "DELETE FROM classes WHERE idcl="+idcl;
        maConnexion.requeteUpdate(req);
    }
}
