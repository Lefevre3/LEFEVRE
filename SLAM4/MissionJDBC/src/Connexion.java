import java.sql.*;
import java.util.ArrayList;

public class Connexion {
    private static Connection conn = null;
    private static String url = "";
    private static String user = "";
    private static String password = "";

    public Connexion(){
    }

    public Connexion(String url,String user,String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }
    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String toString(){
        return this.url + " " + this.user + " " + this.password;
    }

    public void createConnection(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn= DriverManager.getConnection(url,user, password);
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Impossible de créer la connexion");
        }
    }

    public void closeConnection(){
        try {
            conn.close();
        } catch (SQLException throwables) {
            System.out.println("Impossible de fermer la connexion");
        }
    }

    public ArrayList<Classes> requeteSelect(String req){
        ResultSet res = null;
        ArrayList<Classes> tableClasses = new ArrayList<Classes>();
        try {
            Statement stmt = conn.createStatement();
            res = stmt.executeQuery(req);
        } catch (SQLException throwables) {
            System.out.println("Impossible d'effectuer la requête");
        }
        int i=0;
        Classes classesLine;
        try {
            while (res.next()) {
                classesLine = new Classes(res.getInt("idcl"), res.getString("libellecl"));
                tableClasses.add(classesLine);
            }
        } catch (SQLException throwables) {
            System.out.println("La table est vide");
        }
        return tableClasses;
    }

    public void requeteUpdate(String req){
        Statement stmt = null;
        try {
            stmt = conn.createStatement();
            int res2 = stmt.executeUpdate(req);
            System.out.println("nb de modifications réalisées : " + res2);
        } catch (SQLException throwables) {
            System.out.println("La modification n'a pu être effectuée");
        }
    }
}