import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        /*Connection conn = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn= DriverManager.getConnection("jdbc:mysql://localhost/hearthstone?serverTimezone=UTC&useLegacyDatetimeCode=false","admin", "admin");
            Statement stmt = conn.createStatement();
            //Etape 1
            String req1 = "SELECT * FROM classes";
            requeteSelect(stmt, req1);
            String req2 = "INSERT INTO classes VALUES (1, 'abcd')";
            int res2 = stmt.executeUpdate(req2);
            System.out.println("nb de modifications réalisées : " + res2);
            requeteSelect(stmt, req1);
            String req3 = "DELETE FROM classes WHERE idcl=1";
            int res3 = stmt.executeUpdate(req3);
            System.out.println("nb de modifications réalisées : " + res3);
            requeteSelect(stmt, req1);
            //Etape 2

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }*/

        String url = "jdbc:mysql://localhost/hearthstone?serverTimezone=UTC&useLegacyDatetimeCode=false";
        String user = "admin";
        String password = "admin";
        Connexion maConnexion = new Connexion(url, user, password);
        ClassesDAO classesDAO = new ClassesDAO();
        //maConnexion.setUrl(url);
        //maConnexion.setUser(user);
        //maConnexion.setPassword(password);
        maConnexion.createConnection();
        try {
            ArrayList<Classes> tableClasses = classesDAO.recupererALL(maConnexion);
        int count = 0;
        while (tableClasses.size() > count) {
            System.out.println(tableClasses.get(count).toString());
            count++;
        }
        Classes addClasses = new Classes(1, "abcd");
        classesDAO.ajouterClasses(addClasses, maConnexion);
        tableClasses = classesDAO.recupererALL(maConnexion);
        count = 0;
        while (tableClasses.size() > count) {
            System.out.println(tableClasses.get(count).toString());
            count++;
        }
        classesDAO.supprimerClasses("1", maConnexion);
        tableClasses = classesDAO.recupererALL(maConnexion);
        count = 0;
        while (tableClasses.size() > count) {
            System.out.println(tableClasses.get(count).toString());
            count++;
        }
        }finally {
            maConnexion.closeConnection();
        }
        //System.out.print(maConnexion.toString());
    }

    /*    public void requeteSelect(Statement stmt, String req1){
        ResultSet res = null;
        try {
            res = stmt.executeQuery(req1);

        while(res.next()){
            //Retrieve by column name
            int idcl  = res.getInt("idcl");
            String libellecl = res.getString("libellecl");

            //Display values
            System.out.print("ID: " + idcl);
            System.out.println(", libellecl: " + libellecl);
        }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }*/
}
