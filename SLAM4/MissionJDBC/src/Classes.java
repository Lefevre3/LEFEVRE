public class Classes {
    private int idcl;
    private String libellecl;

    public Classes(){
    }

    public Classes(int idcl,String libellecl) {
        this.idcl = idcl;
        this.libellecl = libellecl;
    }

    public int getIdcl() {
        return idcl;
    }
    public void setIdcl(int idcl) {
        this.idcl = idcl;
    }

    public String getLibellecl() {
        return libellecl;
    }
    public void setLibellecl(String libellecl) { this.libellecl = libellecl; }

    public String toString(){
        return this.idcl + " " + this.libellecl;
    }
}
