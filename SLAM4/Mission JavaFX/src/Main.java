import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.util.ArrayList;

public class Main extends Application  {
    private Stage stage;
    Label result = new Label("");
    @Override
    public void start(Stage primaryStage) {
        VBox vBox = new VBox();

        vBox.setSpacing(8);
        vBox.setPadding(new Insets(10,10,10,10));
        Button search = new Button("Rechercher");
        vBox.setAlignment(Pos.CENTER);
        TextField input = new TextField();
        VBox.setMargin(input, (new Insets(10,0,10,0)));;
        vBox.getChildren().addAll(new Label("Identifiant :"), input, search, result);
        vBox.setStyle("-fx-background-color: #fdfd96;");

        VBox.setMargin(result, (new Insets(10,0,0,0)));;

        Scene scene = new Scene(vBox,400,400);
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setTitle("TestJavaFX");

        search.setOnAction(e -> {
            if (input.getText().length()>0) {
                try {
                    result.setTextFill(Color.web("#00ff00"));
                    search(Integer.parseInt(input.getText()));
                }
                catch(Exception ex){
                    result.setTextFill(Color.web("#ff0000"));
                    result.setText("Veuillez entrer un nombre.");
                }
            }else{
                result.setTextFill(Color.web("#ff0000"));
                result.setText("Veuillez saisir un id");
            }
        });

    }

    private void search(int id) {
        String url = "jdbc:mysql://localhost/hearthstone?serverTimezone=UTC&useLegacyDatetimeCode=false";
        String user = "root";
        String password = "";

        try {
            HearthstoneDAO maConnexionDAO = new HearthstoneDAO(url, user, password);
            maConnexionDAO.createConnection();
            String req = "SELECT * FROM classes WHERE idcl="+id;
            ArrayList userData = maConnexionDAO.requeteSelect(req);
            //print list values
            if (userData.size()>0){
                for(int i=0;i<userData.size();i++){
                    result.setText(userData.get(i).toString()+"\n");
                }
            }
            else{
                result.setTextFill(Color.web("#ff0000"));
                result.setText("Pas de résultat.");
            }
        } catch (SQLSyntaxErrorException | ClassNotFoundException thowable) {
            result.setTextFill(Color.web("#ff0000"));
            result.setText("Impossible d'accéder à la base.\n"+thowable);
        }
        catch (SQLException throwables) {
            result.setTextFill(Color.web("#ff0000"));
            result.setText("Erreur de requête à la base.\n"+throwables);
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}