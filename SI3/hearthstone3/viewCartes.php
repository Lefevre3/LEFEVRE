<!DOCTYPE HTML>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="hearthstone.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body style="margin-left: 0px;">
    
<table class="table">
  <thead>
    <tr>
        <th scope="col">Id</th>
        <th scope="col">Attack</th>
        <th scope="col">Rarity</th>
        <th scope="col">Health</th>
        <th scope="col">Cost</th>
        <th scope="col">Faction</th>
        <th scope="col">Name</th>
        <th scope="col">Durability</th>
        <th scope="col">Card class</th>
        <th scope="col">Type</th>
        <th scope="col">Artist</th>
        <th scope="col">Race</th>
    </tr>
  </thead>
  <tbody>
    <tr>
<?php
header('Content-Type: text/html; charset=utf-8');
$dsn='mysql:dbname=hearthstone;host=127.0.0.1';
$user='admin';
$password='admin';

try{
    $dbh=new PDO($dsn,$user,$password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
}catch(PDOException $e){
    echo'Connexion échouée:'.$e->getMessage(); 
}

$sql = "SELECT * FROM cartes";
$resultats = $dbh->prepare($sql);
$resultats->execute(); 
$allresults= $resultats->fetchAll();
$i = 0;
$rar4 = 0;
$rar5 = 0;
$rar6 = 0;
$rar7 = 0;
$rar8 = 0;
$ttcartes = "";
while (isset($allresults[$i][0])) {
    $ttcartes .= '<tr><th scope="row">'.$allresults[$i][0].'</th><td>'.$allresults[$i][1].'</td><td>'.$allresults[$i][2].'</td><td>'.$allresults[$i][3].'</td><td>'.$allresults[$i][4].'</td><td>'.$allresults[$i][5].'</td><td>'.$allresults[$i][6].'</td><td>'.$allresults[$i][7].'</td><td>'.$allresults[$i][8].'</td><td>'.$allresults[$i][9].'</td><td>'.$allresults[$i][10].'</td><td>'.$allresults[$i][11].'</td>';
    if ($allresults[$i][1] == 4) {
        $rar4++;
    }
    if ($allresults[$i][1] == 5) {
        $rar5++;
    }
    if ($allresults[$i][1] == 6) {
        $rar6++;
    }
    if ($allresults[$i][1] == 7) {
        $rar7++;
    }
    if ($allresults[$i][1] == 8) {
        $rar8++;
    }
    $i++;
}
echo "<b>Nombre de cartes : ". $i;
echo "<br>Rareté 4 : ". $rar4;
echo "<br>Rareté 5 : ". $rar5;
echo "<br>Rareté 6 : ". $rar6;
echo "<br>Rareté 7 : ". $rar7;
echo "<br>Rareté 8 : ". $rar8 . "</b>";
echo $ttcartes;
?>

    </tr>
  </tbody>
</table>
    
    
    
    <div style="height: 500px;"></div>
    <?php include 'footer.html' ?>
</body>