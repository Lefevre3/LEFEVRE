
<?php
     $to  = 'alex.lefevre@edu.itescia.fr'; // notez la virgule

     // Sujet
     $subject = 'Contact from index.html';

     // message
     $message = 'Mail : ' . $_POST['mail'] . '<br>Nom : ' . $_POST['nom']. '<br>Message : ' . $_POST['message'];

     // Pour envoyer un mail HTML, l'en-tête Content-type doit être défini
     $headers[] = 'MIME-Version: 1.0';
     $headers[] = 'Content-type: text/html; charset=iso-8859-1';

     // En-têtes additionnels
     $headers[] = 'From: ' . $_POST['mail'];

     // Envoi
     mail($to, $subject, $message, implode("\r\n", $headers));
     header('Location: http://alex-lefevre.fr/');
?>
