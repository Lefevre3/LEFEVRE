<!DOCTYPE HTML>
<html lang="fr">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Portfolio - Alex Lefevre</title>
    <link rel="icon" href="images/sky.png" type="image/png"/>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="https://kit.fontawesome.com/01f400dd40.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="main.js"></script>
</head>
<body>
    <?php if((isset($_GET['m'])) && ($_GET['m']==1)){ ?>
    
    
    <div class="divpopup" id="popup">
        <div class="mypopup">
            <i id="hidepopup" class="fas fa-times-circle" style="position: absolute;top:0;right:0;z-index: 8180;font-size: 2rem;color: #007bff;"></i>
            <div class="popupmessage">
            <div>
                <i class="fas fa-check-circle" style="font-size: 5rem;color:#28a745;"></i>
                <br>
                <h3 class="mt-4">Un mail a bien été envoyé !</h3>
            </div>
            </div>
        </div>
    </div>
    <script>
        $('#hidepopup').on("click", function() {
            $("#popup").hide();
        });
    </script>
    
    
    <?php } ?>
    <nav class="navbar navbar-default navbar-expand-lg navbar-light fixed-top d-flex justify-content-between">
        <a class="navbar-brand d-flex align-items-center ml-5" href="http://www.alex-lefevre.fr/"><b style="font-size:1.7em;">Accueil</b></a>
            <div class="d-none d-lg-block">
            <ul class="navbar-nav" style="font-size:1.2rem;">
                <li class="nav-item text-center align-bottom">
                    <a class="nav-link" href="http://www.alex-lefevre.fr/presentation.php">Présentation de l'entreprise</a>
                </li>
                <li class="nav-itemtext-center">
                    <a class="nav-link" href="http://www.alex-lefevre.fr/situation.php">Situation professionnelle</a>
                </li>
                <li class="nav-item text-center">
                    <a class="nav-link" href="http://www.alex-lefevre.fr/veille.php">Veille technologique</a>
                </li>
                <li class="nav-item text-center">
                    <a class="nav-link" href="http://www.alex-lefevre.fr/analyse.php">Analyse critique</a>
                </li>
            </ul>
            </div>
    </nav>
    <section>
        <div id="navoid" class="p-2"></div>
        <div class="container">
            <div id="accueil" class="text-center h1 mt-3">
                ACCUEIL
            </div>
        </div>
        <div class=" mt-3" id="presentation">
            <div class="text-center container py-5">
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean auctor vulputate enim ut dapibus. Nam congue id magna eget laoreet. Nam quis lobortis est. Ut placerat, felis ac faucibus tristique, ligula sapien posuere nisi, id blandit mi purus in mauris. Nunc posuere, augue ut facilisis mollis, nulla magna malesuada metus, sit amet rhoncus sapien nunc id orci. Duis lobortis convallis lacinia. In bibendum aliquam eleifend. Pellentesque dolor risus, rutrum eget rutrum quis, pulvinar a felis. In elit turpis, luctus et luctus in, eleifend ut sem. Duis scelerisque ipsum quis sem accumsan consequat.<br>
                <button type="button" class="btn btn-outline-light mt-4" id="movetocontact">Contactez-moi</button>
                <a href="https://www.linkedin.com/in/alex-lefevre/" class="ml-2" onmouseover="lkhover()" onmouseout="lkunhover()">
                    <button type="button" class="btn btn-outline-light mt-4" style="height: 37.6px;">
                        <img src="images/linkedin.png" class="h-100 pb-1" id="linkedinimg">
                        Voir mon profil
                    </button>
                </a>
            </div>
        </div>
        <div id="competences" class="container mt-5">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="progress un 90%">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                        <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                        <div class="progress-value">HTML</div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="progress deux 80%">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                        <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                        <div class="progress-value">CSS</div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="progress trois 70%">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                        <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                        <div class="progress-value">JS</div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="progress quatre 95%">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                        <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                        <div class="progress-value">PHP</div>
                    </div>
                </div>
            </div>
        </div>
        <div id="contact" class="mt-5">
            <div class="container">
            <div class="row py-5">
                <div class="col-5 d-none d-md-block">
                    <img src="images/itescia.png" style="max-width: 80%;max-height: 100%;">
                </div>
                <div class="col-12 col-md-7">
                    <form method="post" action="envoi.php" enctype="multipart/form-data" name="formcontact" onsubmit="return submitform()">
                        <div class="col-12 text-center text-warning mb-3 h4" id="erreurmsg" hidden>Tous les champs doivent être remplis</div>
                        <div class="form-row">
                            <div class="col-6">
                                <input type="email" placeholder="Votre addresse mail" class="form-control" name="mail">
                            </div>
                            <div class="col-6">
                                <input type="text" placeholder="Votre nom" class="form-control" name="nom" id="nom">
                            </div>
                        </div>
                        <div class="form-row mt-3">
                            <div class="col-12">
                                <textarea name="message" id="message" rows="5" class="form-control" placeholder="Votre message" style="font-size: 1.2rem;"></textarea>
                            </div>
                        </div>
                        <div class="form-row mt-3 text-center">
                            <div class="col-12">
                                <input type="submit" class="btn btn-warning btn-lg">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            </div>
        </div>
</section>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
<script>
$(window).scroll(function(){
	$('nav').toggleClass('scrolled', $(this).scrollTop() > 50);
});
    
function lkhover() {
  document.getElementById('linkedinimg').setAttribute('src', 'images/linkedinblack.png');
}

function lkunhover() {
  document.getElementById('linkedinimg').setAttribute('src', 'images/linkedin.png');
}
    
$("#movetocontact").click(function() {
    window.scrollTo({
      top: document.body.scrollHeight,
      left: 0,
      behavior: 'smooth'
    });
});

function submitform() {
    var input1 = document.forms['formcontact'].elements[0];
    var input2 = document.forms['formcontact'].elements[1];
    var input3 = document.forms['formcontact'].elements[2];
    if ((input1.value == "") || (input2.value == "") || (input3.value == "")) {
    document.getElementById('erreurmsg').removeAttribute('hidden');
    return false;
    } else {
        return true;
    }
}

</script>
    
</body>
</html>