<?php 
require 'vendor/autoload.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

function connexion(){
    $dsn='mysql:dbname=finder;host=127.0.0.1';
    $user='admin';
    $password='admin';
    return $dbh=new PDO($dsn,$user,$password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
}
function getChambre($id)
{
    $sql = "SELECT * FROM chambre WHERE id=:id";
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->bindParam(":id", $id);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_CLASS); 
        return json_encode($result, JSON_PRETTY_PRINT);
    } catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
    }
}
function getAccount($id, $email)
{
    $sql = "SELECT * FROM accounts WHERE id=:id AND email=:email";
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->bindParam(":id", $id);
        $statement->bindParam(":email", $email);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_CLASS); 
        return json_encode($result, JSON_PRETTY_PRINT);
    } catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
    }
}
    
$app = new \Slim\App;
$app->get('/bonjour', function(Request $request, Response $response){  
  return "bonjour";
});
$app->get('/chambre/{id}', function(Request $request, Response $response){  
    $id = $request->getAttribute('id');
    return getChambre($id);
});
$app->get('/user', function(Request $request, Response $response){  
    $tb = $request->getQueryParams();
    $id = $tb["id"];
    $email = $tb["email"];
    return getAccount($id, $email);
});
$app->run();