<?php

class Plat
{
    
    private $id;
    private $nom;
    private $prix;
    private $accompagnement;
    private $image;
    
    public function __get($name) {
        return $this->$name;
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }
    
    public function __toString()
    {
        return $this->id . ";" . $this->nom . ";" . $this->prix . ";" . $this->accompagnement . ";" . $this->image;
    }
}

?>