<?php

include 'managerInterface.php';
include 'Dessert.php';
include 'DessertDao.php';

$dessertdao=new DessertDao();
$dessertdao->setConnection();
$tb=$dessertdao->findAll();



?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet"> 
</head>
<body class="graybg">
    <nav class="navbar container text-white" style="height: 80px;font-size: 1.25rem;">
        <div class="h-100">
            <img src="images/chef.jpg" class="mw-100 mh-100 mr-3">
            <a class="mr-3 text-white">Accueil</a>
            <a class="text-white">Commander</a>
        </div>
        <div>
            <a href="#" class="text-white">Connexion</a>
        </div>
    </nav>
    <div style="background-color: #FFF;">
    <div id="cont" class="container whitebg text-center">
            <?php $i=1;
            foreach ($tb as $row ) { ?>
            <div class="card d-inline-block py-2" style="width: 30%;">
                <img class="card-img-top" src="./images/<?php echo $row->image; ?>" alt="">
                <div class="card-body">
                    <h5 class="card-title"><?php echo $row->nom; ?></h5>
                    <span hidden>d<?php echo $row->id; ?></span>
                </div>
            </div>
            <?php } ?>
    </div>
    </div>
    </body>
</html>