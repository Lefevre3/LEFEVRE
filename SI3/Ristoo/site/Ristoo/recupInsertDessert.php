<?php

include 'managerInterface.php';
include 'Dessert.php';
include 'DessertDao.php';

$destination = './images/' . basename($_FILES['Image']['name']);
move_uploaded_file($_FILES['Image']['tmp_name'], $destination);
    
$monDessert = new Dessert();
$monDessert->id = $_POST['Id'];
$monDessert->nom = $_POST['Nom'];
$monDessert->prix = $_POST['Prix'];
$monDessert->type = $_POST['Type'];
$monDessert->image = basename($_FILES['Image']['name']);

$dessertdao = new DessertDao();
$dessertdao->setConnection();
$dessertdao->insert($monDessert);

header('Location: ./formInsertDessert.html');

?>  