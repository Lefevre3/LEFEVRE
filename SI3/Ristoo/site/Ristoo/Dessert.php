<?php

class Dessert
{
    
    private $id;
    private $nom;
    private $prix;
    private $type;
    private $image;
    
    public function __get($name) {
        return $this->$name;
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }
    
    public function __toString()
    {
        return $this->id . ";" . $this->nom . ";" . $this->prix . ";" . $this->type . ";" .  $this->image;
    }
}

?>