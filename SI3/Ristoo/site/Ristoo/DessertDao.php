<?php

class DessertDao implements managerInterface {
private $connection;


public function setConnection() {
    $dsn='mysql:dbname=ristoo;host=127.0.0.1';
    $user='admin';
    $password='admin';
    try{
        $dbh=new PDO($dsn,$user,$password); 
        $this->connection=$dbh;
    }catch(PDOException $e){
        echo'Connexion échouée:'.$e->getMessage(); 
    }

}

public function insert( $obj):bool {
    $result=false;
    try {
        $values=explode(";",$obj->__toString());
        $sql="INSERT INTO dessert (id, nom, prix, type, image) VALUES ( '$values[0]' , '$values[1]', '$values[2]', '$values[3]', '$values[4]')";
        echo $sql;
        $nbLignes=$this->connection->exec($sql) ;
        if($nbLignes==1)
          $result=true;

    } catch (PDOException $e) {
        echo'insert échoué:'.$e->getMessage(); 
    }
    return $result;
}
    
public function select(int $id):Dessert {
     $p = new Dessert();
    try {
        $sql="SELECT * FROM dessert where id = '$id'";
        $statement=$this->connection->query($sql,PDO::FETCH_ASSOC) ;
        $row  = $statement -> fetch();
        $p->id=$row['id'];
        $p->nom=$row['nom'];
        $p->prix=$row['prix'];
        $p->type=$row['type'];
        $p->image=$row['image'];
    } catch (PDOException $e) {
        echo'select échoué:'.$e->getMessage(); 
    }
    return $p;
}
    
public function update( $obj):bool {
    $result=false;
    try {
        $id=$obj->id;
        $nom=$obj->nom;
        $prix=$obj->prix;
        $type=$obj->type;
        $image=$obj->image;
        $sql="UPDATE dessert SET nom = '$nom', prix = '$prix', type = '$type', image = '$image' WHERE id = '$id'";
        echo $sql;
        $nbLignes=$this->connection->exec($sql) ;
        if($nbLignes==1)
          $result=true;

    } catch (PDOException $e) {
        echo'update échoué:'.$e->getMessage(); 
    }
    return $result;
}
    
public function delete( $obj):bool {
    $result=false;
    try {
        $id=$obj->id;
        $sql="DELETE FROM dessert WHERE id = '$id'";
        echo $sql;
        $nbLignes=$this->connection->exec($sql) ;
        if($nbLignes==1)
          $result=true;

    } catch (PDOException $e) {
        echo'delete échoué:'.$e->getMessage(); 
    }
    return $result;
}
    
//ici le tableau ne contient pas des objets de type Dessert
public function findAll():array {
    $desserts = null;
    try {
        $sql="SELECT * FROM dessert";
        $statement=$this->connection->query($sql);
        $desserts = $statement->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $e) {
        echo'select all échoué:'.$e->getMessage(); 
    }
    return $desserts;
}
}


?>