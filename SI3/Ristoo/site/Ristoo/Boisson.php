<?php

class Boisson
{
    
    private $id;
    private $nom;
    private $prix;
    private $marque;
    private $glacon;
    private $image;
    
    public function __get($name) {
        return $this->$name;
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }
    
    public function __toString()
    {
        return $this->id . ";" . $this->nom . ";" . $this->prix . ";" . $this->marque . ";" . $this->glacon . ";" . $this->image;
    }
}

?>