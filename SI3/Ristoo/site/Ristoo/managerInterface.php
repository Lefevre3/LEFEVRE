<?php

interface managerInterface {
    function setConnection() ;
    function select(int $id);
    function insert( $obj);
    function update( $obj);
    function delete( $obj): bool;
    function findAll(): array;
}

?>