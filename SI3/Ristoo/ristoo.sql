-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `boisson`;
CREATE TABLE `boisson` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) COLLATE utf8_bin NOT NULL,
  `prix` double NOT NULL,
  `marque` varchar(30) COLLATE utf8_bin NOT NULL,
  `glacon` int(11) NOT NULL,
  `image` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `boisson` (`id`, `nom`, `prix`, `marque`, `glacon`, `image`) VALUES
(1,	'coca',	5,	'coca cola',	0,	'coca1.png'),
(2,	'coca sans sucres',	4,	'coca cola',	0,	'coca2.png'),
(3,	'coca cherry',	6,	'coca cola',	0,	'coca3.png'),
(4,	'sprite',	7,	'sprite',	1,	'sprite.png'),
(5,	'fanta orange',	5,	'fanta',	0,	'fanta.png'),
(6,	'pulco citronnade',	8,	'pulco',	1,	'pulco.png'),
(7,	'oasis tropical',	7,	'oasis',	1,	'oasist.png'),
(8,	'evian',	3,	'evian',	0,	'evian.jpg'),
(9,	'badoit',	3,	'badoit',	1,	'badoit.jpg'),
(10,	'oasis pomme',	6,	'oasis',	0,	'oasisp.jpg');

DROP TABLE IF EXISTS `client`;
CREATE TABLE `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(30) COLLATE utf8_bin NOT NULL,
  `password` varchar(30) COLLATE utf8_bin NOT NULL,
  `tel` varchar(30) COLLATE utf8_bin NOT NULL,
  `nom` varchar(30) COLLATE utf8_bin NOT NULL,
  `prenom` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `client` (`id`, `email`, `password`, `tel`, `nom`, `prenom`) VALUES
(1,	'henry@mail.com',	'abcdefgh',	'0154545454',	'ford',	'henry'),
(2,	'john@mail.com',	'agnetrhrh',	'0125252525',	'kennedy',	'john'),
(3,	'robert@mail.com',	'thrthrhrhreth',	'0198854758',	'morris',	'robert'),
(4,	'benjamin@mail.com',	'rthyjdfzefe',	'0165957853',	'franklin',	'benjamin'),
(5,	'thomas@mail.com',	'rttrhrhtrhcvqrf',	'0147584578',	'jefferson',	'thomas'),
(6,	'george@mail.com',	'otriohribr',	'0154756985',	'washington',	'george');

DROP TABLE IF EXISTS `commande`;
CREATE TABLE `commande` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


DROP TABLE IF EXISTS `comptes`;
CREATE TABLE `comptes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(60) COLLATE utf8_bin NOT NULL,
  `password` varchar(60) COLLATE utf8_bin NOT NULL,
  `date` varchar(60) COLLATE utf8_bin NOT NULL,
  `tel` varchar(60) COLLATE utf8_bin NOT NULL,
  `nom` varchar(60) COLLATE utf8_bin NOT NULL,
  `prenom` varchar(60) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


DROP TABLE IF EXISTS `dessert`;
CREATE TABLE `dessert` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) COLLATE utf8_bin NOT NULL,
  `prix` double NOT NULL,
  `type` varchar(30) COLLATE utf8_bin NOT NULL,
  `image` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `dessert` (`id`, `nom`, `prix`, `type`, `image`) VALUES
(1,	'churros',	6,	'sucre',	'churros.jpg'),
(2,	'caramel bread',	7,	'sucre',	'cbread.jpg'),
(3,	'choco bread',	7,	'sucre',	'chbread.jpg'),
(4,	'moelleux',	5,	'sucre',	'moelleux.jpg'),
(5,	'bavarois',	8,	'sale',	'bavarois.jpg'),
(6,	'tiramisu caramel',	7,	'sale',	'tiramisu.jpg'),
(7,	'tarte caramel',	6,	'sale',	'tarte.jpg'),
(8,	'glace chocolat',	5,	'sucre',	'choco.png'),
(9,	'glace pomme',	5,	'sucre',	'pomme.png'),
(10,	'glace vanille',	5,	'sucre',	'vanille.png'),
(15,	'EssaiInsert',	25,	'sucre',	'trutruc.jpg');

DROP TABLE IF EXISTS `plat`;
CREATE TABLE `plat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) COLLATE utf8_bin NOT NULL,
  `prix` double NOT NULL,
  `accompagnement` varchar(30) COLLATE utf8_bin NOT NULL,
  `image` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `plat` (`id`, `nom`, `prix`, `accompagnement`, `image`) VALUES
(1,	'nuggets',	11,	'frite',	'nuggets.jpg'),
(2,	'nuggets',	11,	'potatoes',	'nuggets.jpg'),
(3,	'poulet roti',	12,	'salade',	'poulet.jpg'),
(4,	'burger',	13,	'frite',	'burger.jpg'),
(5,	'burger',	13,	'potatoes',	'burger.jpg'),
(6,	'canard',	15,	'haricots',	'canard.jpg'),
(7,	'pizza saumon',	14,	'creme',	'saumon.jpg'),
(8,	'pizza royale',	12,	'tomate',	'royale.jpg'),
(9,	'pizza raclette',	12,	'creme',	'raclette.jpg'),
(10,	'pizza raclette',	12,	'tomate',	'raclette.jpg');

DROP TABLE IF EXISTS `supplement`;
CREATE TABLE `supplement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `boisson` int(11) NOT NULL,
  `dessert` int(11) NOT NULL,
  `plat` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `boisson` (`boisson`),
  KEY `dessert` (`dessert`),
  KEY `plat` (`plat`),
  CONSTRAINT `supplement_ibfk_1` FOREIGN KEY (`boisson`) REFERENCES `boisson` (`id`),
  CONSTRAINT `supplement_ibfk_2` FOREIGN KEY (`dessert`) REFERENCES `dessert` (`id`),
  CONSTRAINT `supplement_ibfk_3` FOREIGN KEY (`plat`) REFERENCES `plat` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


-- 2020-05-18 09:25:15
