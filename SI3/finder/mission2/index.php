<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet"> 
</head>
<body class="graybg">
    <nav class="navbar container text-white" style="height: 80px;font-size: 1.25rem;">
        <div class="h-100">
            <img src="img/logo.png" class="mw-100 mh-100 mr-3">
            <a class="mr-3 text-white">Accueil</a>
            <a class="text-white">Commander</a>
        </div>
        <div>
            <a href="#" class="text-white">Connexion</a>
        </div>
    </nav>
    <div style="background-color: #FFF;">
    <div id="app" class="container whitebg">
        <template>
            <div class="table-responsive">
                <table class="table-hover my-4 mx-auto" v-if="info">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre de couchage</th>
                            <th>Porte</th>
                            <th>Etage</th>
                            <th>Categorie</th>
                            <th>Baignoire</th>
                            <th>Prix</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(item, idx) in info" class="text-center">
                            <td>{{ item.id }}</td>
                            <td>{{ item.nbCouchage }}</td>
                            <td>{{ item.porte }}</td>
                            <td>{{ item.etage }}</td>
                            <td>{{ item.idCategorie }}</td>
                            <td>{{ item.baignoire }}</td>
                            <td>{{ item.prixBase }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </template>
    </div>
    </div>
<script>
new Vue({
  el: '#app',
  data () {
    return {
      info: null
    }
  },
  mounted () {
        axios({
          method: 'get',
          url: 'http://help.frebourg.es/api/index.php/chambres',
          responseType: 'json'
        })
       .then(async res => { 
         if(res.status === 200) {
               this.info=res.data;
          } else if(res.status === 400) { 
              let errorResponse = await res.json(); this.errors.push(errorResponse.error); }
       });
  }
})
</script>
    </body>
</html>