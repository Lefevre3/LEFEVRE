<?php 
require 'vendor/autoload.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

function connexion(){
    $dsn='mysql:dbname=alexfinder_finder;host=mysql-alexfinder.alwaysdata.net';
    $user='234108';
    $password='finderadmin';
    return $dbh=new PDO($dsn,$user,$password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
}
function getChambre($id)
{
    $sql = "SELECT * FROM chambre WHERE id=:id";
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->bindParam(":id", $id);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_CLASS); 
        return json_encode($result, JSON_PRETTY_PRINT);
    } catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
    }
}
function getChambres()
{
    $sql = "SELECT * FROM chambre";
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_CLASS); 
        return $result;
    } catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
    }
}
function getAccount($id, $nom)
{
    $sql = "SELECT * FROM accounts WHERE id=:id AND nom=:nom";
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->bindParam(":id", $id);
        $statement->bindParam(":nom", $nom);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_CLASS); 
        return json_encode($result, JSON_PRETTY_PRINT);
    } catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
    }
}

function insertUser($id, $nom)
{
    $sql = "INSERT INTO `accounts` (`id`, `email`, `password`, `nom`) VALUES (:id, :email, '', :nom)";
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->bindParam(":id", $id);
        $statement->bindParam(":nom", $nom);
        $statement->bindParam(":email", $email);
        $statement->execute();
        return "réussi";
    } catch(PDOException $e){
        return '{"error":'.$e->getMessage().'}';
    }
}

function deleteUser($prenom, $nom)
{
    $sql = "DELETE FROM user WHERE nom=:nom AND prenom=:prenom";
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->bindParam(":prenom", $prenom);
        $statement->bindParam(":nom", $nom);
        $statement->execute();
        if( ! $statement->rowCount() ) return "error";
        return "réussi";
    } catch(PDOException $e){
        return 'error';
    }
}

function updateUser($prenom, $email)
{
    $sql = "UPDATE user SET email=:email WHERE prenom=:prenom";
    try{
        $dbh=connexion();
        $statement = $dbh->prepare($sql);
        $statement->bindParam(":prenom", $prenom);
        $statement->bindParam(":email", $email);
        $statement->execute();
        if( ! $statement->rowCount() ) return "error";
        return "réussi";
    } catch(PDOException $e){
        return 'error';
    }
}
    
$app = new \Slim\App;
$app->get('/bonjour', function(Request $request, Response $response){  
  return "bonjour";
});
$app->get('/chambre/{id}', function(Request $request, Response $response){  
    $id = $request->getAttribute('id');
    return getChambre($id);
});
$app->get('/chambres', function(Request $request, Response $response){
    return $response->withJson(getChambres(),200);
});
$app->get('/user', function(Request $request, Response $response){  
    $tb = $request->getQueryParams();
    $id = $tb["id"];
    $nom = $tb["nom"];
    return getAccount($id, $nom);
});
$app->delete('/user', function(Request $request, Response $response){  
    $tb = $request->getParsedBody();
    $prenom = $tb["prenom"];
    $nom = $tb["nom"];
    if(deleteUser($prenom, $nom)=="réussi"){
        return $response->withStatus(200);
    }else{
        return $response->withStatus(401);
    }
});
$app->put('/user', function(Request $request, Response $response){  
    $tb = $request->getParsedBody();
    $prenom = $tb["prenom"];
    $email = $tb["email"];
    if(updateUser($prenom, $email)=="réussi"){
        return $response->withStatus(200);
    }else{
        return $response->withStatus(401);
    }
});
$app->post('/user', function(Request $request, Response $response){
    $tb = $request->getQueryParams(); 
    $id = $tb["id"];
    $nom = $tb["nom"];
    $email = $tb["email"];
    return insertUser($id, $nom, $email);
});
$app->run();