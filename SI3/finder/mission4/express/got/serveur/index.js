var pg = require('pg');

var express = require('express');
var bodyParser = require('body-parser');
var cors= require('cors');
//var hostname = 'localhost'; 
//var port = 8080;
var hostname = (process.env.VCAP_APP_HOST || 'localhost');
var port = (process.env.VCAP_APP_PORT || 3000);
var app = express();
app.use(cors());
app.use(bodyParser());

var connectionString = "pg://alexfinder:finderadmin@postgresql-alexfinder.alwaysdata.net:5432/alexfinder_finder";
var connection= new pg.Client(connectionString);
connection.connect(); 

app.get('/chambre/:id',function(req,res,next){ 
  id= req.params.id;
  connection.query('SELECT * from chambre WHERE id='+id, function (error, results, fields) {
       if (error) throw error; res.json({ message : results}); }); 
})

app.get('/user/:mail&:mdp',function(req,res,next){ 
    mail= req.params.mail;
    mdp= req.params.mdp;
    connection.query("SELECT * from utilisateur WHERE email='"+mail+"' AND motdepasse='"+mdp+"'", function (error, results, fields) {
        if (error) throw error;
        if(results['rowCount']==1){
            res.status(200).send();
        }else{
            res.status(401).send();
        }
    });  
})

app.post('/user',function(req,res,next){
    console.log(req.body);
    mail= req.body.mail;
    mdp= req.body.mdp;
    nom= req.body.nom;
    prenom= req.body.prenom;
    connection.query("INSERT INTO utilisateur(email, nom, prenom, motdepasse) VALUES ('"+mail+"', '"+nom+"', '"+prenom+"', '"+mdp+"')", function (error, results, fields) {
        if (error) throw error;
        res.json({ message : results});
    });  
})

app.get('/chambres',function(req,res,next){ 
    connection.query("SELECT * FROM chambre", function (error, results, fields) {
        if (error) throw error;
        if(results['rowCount']>=1){
            res.status(200).json({ message : results.rows});
        }else{
            res.status(401).send();
        }
    });
})

app.put('/user',function(req,res,next){ 
    prenom= req.body.prenom;
    email= req.body.email;
    connection.query("UPDATE utilisateur SET email= '"+email+"' WHERE prenom='"+prenom+"'", function (error, results, fields) {
        if (error) throw error;
        if(results['rowCount']==1){
            res.status(200).send();
        }else{
            res.status(401).send();
        }
    });
})

app.delete('/user',function(req,res,next){ 
    prenom= req.body.prenom;
    nom= req.body.nom;
    connection.query("DELETE FROM utilisateur WHERE nom='"+nom+"' AND prenom='"+prenom+"'", function (error, results, fields) {
        if (error) throw error;
        if(results['rowCount']==1){
            res.status(200).send();
        }else{
            res.status(401).send();
        }
    });
})

app.get('/bonjour', function(req, res){ 
    res.json({message : "Youhou", methode : req.method});
})

// Démarrer le serveur 
app.listen(port, hostname, function(){
  console.log("Mon serveur fonctionne sur http://"+ hostname +":"+port); 
});