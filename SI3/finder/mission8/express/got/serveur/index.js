var pg = require('pg');
var jwt = require('jsonwebtoken');
var express = require('express');
var bodyParser = require('body-parser');
var cors= require('cors');
//var hostname = 'localhost'; 
//var port = 8080;
var hostname = (process.env.VCAP_APP_HOST || 'localhost');
var port = (process.env.VCAP_APP_PORT || 3020);
var app = express();
app.use(cors());
app.use(bodyParser());

function verifToken(token){
    var b =false;
    if (token!=null) {
        // verifies secret and checks exp
        b=jwt.verify(token, 'superSecret', function(err, decoded) {      
            if (err) {
                console.log(err);
            } else {
            // if everything is good, save to request for use in other routes
            return true;
            }
         });
   
    }
    return b;
}

var connectionString = "pg://alexfinder:finderadmin@postgresql-alexfinder.alwaysdata.net:5432/alexfinder_finder";
var connection= new pg.Client(connectionString);
connection.connect(); 

app.get('/chambre/:id',function(req,res,next){ 
  id= req.params.id;
  connection.query('SELECT * from chambre WHERE id='+id, function (error, results, fields) {
       if (error) throw error; res.json({ message : results}); }); 
})

app.get('/user/:mail&:mdp',function(req,res,next){ 
    mail= req.params.mail;
    mdp= req.params.mdp;
    connection.query("SELECT * from utilisateur WHERE email='"+mail+"' AND motdepasse='"+mdp+"'", function (error, results, fields) {
        if (error) throw error;
        if(results['rowCount']==1){
            res.status(200).send();
        }else{
            res.status(401).send();
        }
    });  
})

app.post('/user',function(req,res,next){
    mail= req.body.mail;
    mdp= req.body.mdp;
    nom= req.body.nom;
    prenom= req.body.prenom;
    connection.query("INSERT INTO utilisateur(email, nom, prenom, motdepasse) VALUES ('"+mail+"', '"+nom+"', '"+prenom+"', '"+mdp+"')", function (error, results, fields) {
        if (error) throw error;
        res.json({ message : results});
    });  
})

app.get('/chambres',function(req,res,next){ 
    connection.query("SELECT * FROM chambre", function (error, results, fields) {
        if (error) throw error;
        if(results['rowCount']>=1){
            res.status(200).json({ message : results.rows});
        }else{
            res.status(401).send();
        }
    });
})

app.put('/user',function(req,res,next){ 
    prenom= req.body.prenom;
    email= req.body.email;
    token=req.body.token;
    var resi=verifToken(token);
    if(resi){
        connection.query("UPDATE utilisateur SET email= '"+email+"' WHERE prenom='"+prenom+"'", function (error, results, fields) {
            if (error) throw error;
            if(results['rowCount']==1){
                res.status(200).send();
            }else{
                res.status(401).send();
            }
        });
    }else{
        res.status(401).send();
    }
})

app.delete('/user',function(req,res,next){ 
    prenom= req.body.prenom;
    nom= req.body.nom;
    token=req.body.token;
    var resi=verifToken(token);
    if(resi){
        connection.query("DELETE FROM utilisateur WHERE nom='"+nom+"' AND prenom='"+prenom+"'", function (error, results, fields) {
            if (error) throw error;
            if(results['rowCount']==1){
                res.status(200).send();
            }else{
                res.status(401).send();
            }
        });
    }else{
        res.status(401).send();
    }
})

app.get('/bonjour', function(req, res){ 
    res.json({message : "Youhou", methode : req.method});
})

app.post('/obtentionToken', function(req, res){
    login= req.body.login;
    mdp= req.body.password;
    connection.query("SELECT * from utilisateur WHERE email='"+login+"' AND motdepasse='"+mdp+"'", function (error, results, fields) {
        if (error) throw error;
        if(results['rowCount']==1){
            var token = jwt.sign({ loginpass: login+mdp },'superSecret', {
                  expiresIn: 1440 // expires in 24 hours
            });
                // return the information including token as JSON
            res.json({
              success: true,
              message: 'Enjoy your token!',
              token: token
            }).status(200).send();
        }else{
            res.status(401).send();
        }
    });
})

app.post('/verifToken',function(req,res,next){
    token= req.body.token;
    if (token) {
    // verifies secret and checks exp
    jwt.verify(token, 'superSecret', function(err, decoded) {      
      if (err) {
        return res.status(401).send();    
      } else {
        // if everything is good, save to request for use in other routes
        return res.status(200).send();
      }
    });
  } else {
    // if there is no token
    return res.status(401).send();
  } 
})

// Démarrer le serveur 
app.listen(port, hostname, function(){
  console.log("Mon serveur fonctionne sur http://"+ hostname +":"+port); 
});