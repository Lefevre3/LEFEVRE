<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reservation;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('createReservation');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    public function store(Request $request)
    {
        $validatedData = $request->validate([
             'dateD' => 'required|date|after:tomorrow',
             'dateF' => 'required|date|after:datef',
             'idPeriode'=> 'required|between:1,3'
        ]);
        $reservation=new Reservation();
        $dateD=$request->input('dateD');
        $dateF=$request->input('dateF');
        $idPeriode=$request->input('idPeriode');
        $date=strtotime("now");
        $reservation->dateD=$dateD;
        $reservation->dateF=$dateF;
        $reservation->idPeriode=$idPeriode;
        $reservation->created_at=$date;
        $reservation->updated_at=$date;
        $reservation->save();
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
