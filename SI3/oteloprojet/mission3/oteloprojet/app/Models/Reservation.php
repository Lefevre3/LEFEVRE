<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

Class Reservation extends Model
{
     //protected $table = "chambre"; 
     //protected $fillable = [ 'nbCouchage' ,'porte','etage','idCategorie','baignoire','prixBase' ];
    protected $nbCouchage;
    protected $porte;
    protected $etage;
    protected $idCategorie;
    protected $baignoire;
    protected $prixBase;
}
