@extends('layouts.app')
@section('content')
<div class="container">
<form method="post" action="{{ route('reservation.store')}}">
@csrf
  <div class="form-group">
    <label for="dateD">dateD</label>
    <input type="text" class="form-control" id="dateD" aria-describedby="dateD" name="dateD">
        @error('dateD')
        <div class="alert alert-danger mt-2">
        {{$message}} mon message perso
        </div>
        @enderror
  </div>
  <div class="form-group">
    <label for="dateF">dateF</label>
    <input type="text" class="form-control" id="dateF" aria-describedby="dateF" name="dateF">
  </div>
    @error('dateF')
        <div class="alert alert-danger mt-2">
        {{$message}} mon message perso
        </div>
        @enderror
  <div class="form-group">
    <label for="idPeriode">idPeriode</label>
    <input type="text" class="form-control" id="idPeriode" aria-describedby="idPeriode" name="idPeriode">
  </div>
    @error('idPeriode')
        <div class="alert alert-danger mt-2">
        {{$message}} mon message perso
        </div>
        @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
@stop