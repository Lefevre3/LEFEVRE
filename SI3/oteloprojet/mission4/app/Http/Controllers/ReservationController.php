<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reservation;



class ReservationController extends Controller
{

   
    
    public function create(){
    return view('createReservation');

    }
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
{

    $validatedData = $request->validate([
        'dateD' => 'required|date|after:tomorrow',
        'dateF' => 'required|date|after:dated',
        'idPeriode'=> 'required|between:1,3'
  ]);

    $reservation=new Reservation();
     $dated=$request->input('dateD');
     $datef=$request->input('dateF');
     $idperiode=$request->input('idPeriode');
     $reservation->dateD=$dated;
     $reservation->dateF=$datef;
     $reservation->idPeriode=$idperiode;
     $reservation->save();
     return redirect()->back();

    
    
}
}

