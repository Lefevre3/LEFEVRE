@extends('layouts.app')

@section('content')
<table class="table table-hover table-sm">
    <thead class="thead-dark">
        <tr>
            <th> id</th>
            <th> nbCouchage</th>
            <th> porte  </th>
            <th> etage </th>
            <th> idCategorie</th>
            <th> baignoire </th>
        </tr>
    </thead>
    <tbody>
   

         @foreach($chambresPremium as $chambresPremium)
          <tr>
          <td> {{$chambresPremium->id}} </td>
              <td> {{$chambresPremium->nbCouchage}} </td>
              <td> {{$chambresPremium->porte}} </td>
              <td> {{$chambresPremium->etage}} </td>
              <td> {{$chambresPremium->idCategorie}} </td>
              <td> {{$chambresPremium->baignoire}} </td>
             
          </tr>
         @endforeach
   </tbody>
</table>
@stop