@extends('layouts.app')

@section('content')
<table class="table table-hover table-sm">
    <thead class="thead-dark">
        <tr>
            <th> id</th>
            <th> nbCouchage</th>
            <th> porte  </th>
            <th> etage </th>
            <th> idCategorie</th>
            <th> baignoire </th>
        </tr>
    </thead>
    <tbody>
   

         @foreach($chambres as $chambre)
          <tr>
          <td> {{$chambre->id}} </td>
              <td> {{$chambre->nbCouchage}} </td>
              <td> {{$chambre->porte}} </td>
              <td> {{$chambre->etage}} </td>
              <td> {{$chambre->idCategorie}} </td>
              <td> {{$chambre->baignoire}} </td>
             
          </tr>
         @endforeach
   </tbody>
</table>
@stop