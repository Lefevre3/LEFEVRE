<?php

namespace App\Models;

use App\Models\Chambre;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

Class Chambre extends Model
{
    use HasFactory;
     protected $table = "chambre"; 
     protected $fillable = [
        'nbCouchage' ,'porte','etage','idCategorie','baignoire','prixBase' 
    ];
}