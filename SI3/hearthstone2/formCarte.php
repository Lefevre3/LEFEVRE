<?php
session_start();
if ((!isset($_SESSION['trouve'])) || ($_SESSION['trouve'] != "abc")) {
    header('Location:formLogin.php');
    exit;
}
?>
<!DOCTYPE HTML>
<head>
    <meta charset="utf-8"/>
</head>
<body>
    <form method="post" action="recupCarte.php">
        <input type="text" placeholder="Id" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name="id" required>
        <input type="text" placeholder="Attack" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name="attack">
        <input type="text" placeholder="rarity" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name="rarity">
        <input type="text" placeholder="health" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name="health">
        <input type="text" placeholder="cost" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name="cost">
        <input type="text" placeholder="faction" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name="faction">
        <input type="text" placeholder="name" name="name">
        <input type="text" placeholder="durability" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name="durability">
        <input type="text" placeholder="card_class" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name="card_class">
        <input type="text" placeholder="type" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name="type">
        <input type="text" placeholder="artist" name="artist">
        <input type="text" placeholder="race" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" name="race">
        <input type="submit">
    </form>
</body>