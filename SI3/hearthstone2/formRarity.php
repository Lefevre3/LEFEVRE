<?php
session_start();
if ((!isset($_SESSION['trouve'])) || ($_SESSION['trouve'] != "abc")) {
    header('Location:formLogin.php');
    exit;
}
?>
<!DOCTYPE HTML>
<head>
    <meta charset="utf-8"/>
</head>
<body>
    <form method="post" action="recupRarity.php">
        <input type="text" placeholder="IDR" name="idr" required>
        <input type="text" placeholder="LIBELLE" name="lib" required>
        <input type="text" placeholder="Couleur" name="coul" required>
        <input type="date" name="date" required>
        <input type="submit">
    </form>
</body>