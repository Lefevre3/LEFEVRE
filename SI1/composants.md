Robin Teissedre et Alex Lefevre

# Ventilateur

## Fixe
![fixe](Images/ventilateur_pc.jpg)

## Portable
![port](Images/ventilateur_port.jpg)

**Le ventilateur d'ordinateur fixe est approximativement trois fois plus gros que celui de l'ordinateur portable.**  
  
*Un ventilateur d'ordinateur est un ventilateur installé à l'intérieur d'un ordinateur et utilisé pour refroidir activement le boîtier en permettant un flux d'air frais.*

# Ports PCI

## Fixe
![fixe](Images/ports_pci_pc.jpg) 

## Portable
![port](Images/port_pci_port.jpg)  

*L'interface PCI est un standard de bus local permettant de connecter des cartes d'extension sur la carte mère d'un ordinateur. L'un des intérêts du bus PCI est que deux cartes PCI peuvent dialoguer entre elles sans passer par le processeur.* 

# Barette de ram et emplacement

## Fixe
![fixe](Images/ram_pc.jpg)  

## Portable
![port](Images/ram_comp.jpg)  

**Une barette de ram d'ordinateur fixe à côté d'un emplacement pour barette de ram d'ordinateur portable**  

*La RAM est un type de mémoire qui équipe tout ordinateur et qui permet de stocker des informations provisoires. Son but n'étant pas de ranger de l'information mais d'y accéder rapidement et provisoirement.*

# Processeur et emplacement

## Comparaison
![comp](Images/processeurs.jpg)  
  
**A gauche le processeur d'un ordinateur portable, à droite le processeur d'un ordinateur fixe.**

*Le processeur est le cerveau, c'est lui qui organise les échanges de données entre les différents composants*