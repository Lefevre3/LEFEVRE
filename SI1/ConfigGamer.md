﻿12/09/2019  
Robin Teissedre et Alex Lefevre  
  
Activité 1 : Configuration d'un ordinateur  
  
Configuration 1 :  
==
  
Site d'achat :  
-
  
<a href="https://urlz.fr/aCY4">https://urlz.fr/aCY4</a>    
<a href="https://urlz.fr/aCY1">https://urlz.fr/aCY1</a>  
<br/>

Composants :
-

- Intel Core i5-9400F (2.9 GHz) 174.90 € - *Processeur*    
- MSI B360 GAMING PRO CARBON 139.90 € - *Carte mère*  
- DDR4 Ballistix Sport LT, Rouge, 8 Go, 2666 MHz, CAS 16 46.90 € - *Barre mémoire*  
- Be Quiet ! DC1, 3 g 5.90 € - *Pâte thermique*  
- MSI GeForce GT 1030 AERO ITX 2GD4 OC, 2 Go 79.90 € - *Carte graphique*  
- Western Digital WD Blue, 1 To 45.90 € - *Disque dur*  
- Zalman T6, Noir 33.90 € - *Boîtier*  
- Cooler Master K450, 450W 32.90 € - *Alimentation*  
- Philips 243V5LHSB + Spirit of Gamer PRO-H5 - Rouge 89.90 € - *Ecran (+casque-micro)*  
- MICROSOFT Windows 10 PackFamille (version complète) 20 € - *Système d'exploitation*  
<br/>  
**Prix total : 670.10 €**  
<br/>   
**Logiciels :** DirectX 12 (Inclus dans Windows) et Java version 8 Update 221  
**Sites :** <a href="https://www.java.com/fr/download/">https://www.java.com/fr/download/</a>  
  
Explication :   
==
  
1. Le processeur et la carte graphique sont légérement au dessus de la configuration recommandée pour Fortnite, ce qui permettra d'ouvrir Discord/Internet en parallèle sans problèmes.  
  
2. La carte mère est compatible avec les autres composants et de bonne qualité.  
  
3. La barre mémoire offre 8go de RAM (Mémoire vive) ce qui permet de faire tourner presque tous les jeux PC actuels. Puisqu'il n'y a qu'une seule barre, il sera possible d'en rajouter une autre si besoin.  
  
4. Le disque dur dispose de 1To pour installer les jeux et toutes les applications requises.  
  
5. Le boîtier est suffisamment grand pour rajouter des composants si vous souhaitez améliorer votre configuration.  
  
6. L'alimentation est plus puissante que ce qui est requis, ce qui lui permet d'être silencieuse.  
  
7. L'écran permet d'avoir la meilleure résolution et est d'une taille satisfaisante pour profiter pleinement des jeux. Il dispose aussi d'un temps de réponse de seulement 1ms (le plus rapide possible) pour éviter les latences. Il y a aussi un casque-micro inclus pour parler avec ses amis et pouvoir entendre si les bruits (de tirs par exemple) viennent de la droite ou de la gauche. 
  
8. Le système d'exploitation Windows est le dernier sorti ce qui assure sa compatibilité avec les jeux actuels et futurs.  
  
9. Il n'y a pas de ventilateur indépendant, cependant les composants le nécessitant en possèdent déjà un. Il est possible d'en rajouter un pour moins de 30 euros (Par exemple "Deepcool Ice Edge Mini FS V2" à 15 euros).  
  
10. Il n'y a pas de carte WiFi puisque l'ordinateur reste à la même place et peut être connecté à Internet grâce à un cable ethernet RJ45.      
  
  
*Etant donné que l'ordinateur coûte 30 € de moins que prévu initialement, il est possible de les investir dans un composant de votre choix (Ventilateur, carte WiFi etc...).*   

	

