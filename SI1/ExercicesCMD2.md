﻿10/10/2019
Alex Lefevre

## Exercices CMD2

# Exercice 1 :

1) Quel est le chemin absolu pour référencer le fichier track1.mp3 ?
drives/I/home/bob/music/rock/track1.mp3

2) Si le répertoire courant est /home/bob, quel est le chemin relatif vers track1.mp3 ?
music/rock/track1.mp3

3) Si le répertoire courant est /home/bob/images, quel est le chemin relatif vers track1.mp3 ?
../music/rock/track1.mp3

4) Si le répertoire courant est /home/bob/tmp/tests, quel est le chemin relatif vers track1.mp3 ?
../../music/rock/track1.mp3

Trouvez toutes les commandes permettant de se déplacer :
1. Dans le répertoire rock : cd /home/bob/music/rock/  - cd rock/
2. Dans le répertoire tests : cd /home/bob/tmp/tests/ - cd ../tmp/tests/
3. Dans le répertoire personnel de bob : cd /home/bob/  - cd ..
4. A la racine de l’arborescence : cd /   - cd ../../../
5. Dans le répertoire etc : cd /etc/  - cd ../../../etc


# Exercice 2 :

Première partie :

Faites la liste de toutes les commandes à exécuter dans l’ordre pour obtenir le résultat
ci-dessus :
-cd /home/User
-mkdir monsite
-cd monsite/
-mkdir css
-mkdir images
-mkdir lib
-mkdir lib/bootstrap
-echo > admin.html
-echo > index.html
-echo > css/style.css


1. Afficher le chemin du répertoire courant : 
pwd

2. Créer un fichier nommé .gitignore dans le répertoire monsite :
echo > /home/user/monsite/.gitignore

3. Afficher le contenu du répertoire monsite avec des informations détaillées : 
ls -l

4. Afficher le contenu du répertoire monsite avec des informations détaillées et
en incluant les fichiers cachés :
ls -al

5. Copier le fichier index.html sous le nom index2.html :
cp index.html index2.html ou cp /home/user/monsite/index.html /home/user/monsite/index2.html

6. Créer un répertoire nommé archive dans le répertoire monsite :
mkdir /home/user/monsite/archive

7. Copier en une seule commande tous les fichiers HTML dans le répertoire
archive :
cp ./*html /home/user/monsite/archive

8. Renommer le fichier style.css en monsite.css :
mv /home/user/monsite/css/style.css /home/user/monsite/css/monsite.css

9. Supprimer en une seule commande les fichiers index.html et index2.html
présents dans monsite :
rm /home/user/monsite/index.html /home/user/monsite/index2.html

10. Supprimer en une seule commande le répertoire monsite et tout son contenu.
rm -rf /home/user/monsite


# Exercice 3

(Pour aller dans répertoire courant : cd /home/user/)
mkdir exo3
cd exo3
echo > moi.txt
echo Je suis en BTS SIO
echo Je suis en BTS SIO > moi.txt
echo Et j’aime ça ! >> moi.txt


Dupliquer rois.txt sous le nom roisFrance.txt :
cp rois.txt roisFrance.txt

Déplacer tous les fichiers sauf rio.txt dans votre répertoire personnel : 
mv $(ls | grep -v rio.txt) /home/User/

Revenir dans votre répertoire personnel : 
cd /home/User/

Supprimer les fichiers créés précédemment : 
rm rois.txt roisFrance.txt

Supprimer le répertoire exo3 : 
rm -rf exo3/

Réinitialiser le contenu de la console : 
clear


# Exercice 4  

Créer le répertoire bin dans votre répertoire personnel : 
mkdir /home/User/bin

Afficher la liste des variables d’environnement : 
export -p

Afficher le nom de l’utilisateur courant : 
whoami

Ajouter le répertoire bin créé précédemment à la variable PATH : 
export PATH=$PATH:/home/User/bin

Créer l’alias la qui lance la commande ls -a :
alias la='ls -a'
