﻿Alex Lefevre 25/10/2019     

|Commandes linux   £ Commandes CMD   £ Utilité:  | Exemple ou autres commandes à même fonction  |
|---|---|
|touch £ >  £ Créer un nouveau fichier | touch /home/oracle/1.txt pour créer fichier nommé 1.txt .Autrement, modifiera la date d'accès du fichier.   |
| | |
|Env £ SET   £ Affiche les variables d'environnements   |env [OPTION]... [-][NAME=VALUE]... [COMMAND [ARG]...]   |
| | |
|cd   £ cd   £ Affiche le répertoire actuel et vous permet de passer à d’autres répertoires.   | cd /home/oracle pour aller dans le répertoire /home/oracle. LINUX : chdir   |
| | |
|ls   £ dir   £ Affiche tous les dossiers et fichiers du répertoire courant   | ls /home/oracle pour lister le contenu du répertoire oracle.|
| | |
|cp   £ copy/xcopy  £ copie un ou plusieurs fichiers   |CMD :"robocopy" Permet la copie de fichiers « robuste ». Avec robocopy, il est possible de transférer des données avec succès même en cas d’interruption du réseau.|
| | |
|rm    £ del   £Supprime un ou plusieurs fichiers.   | rm myfile.txt   |
|rm -rf   £ Deltree   £ Supprime un répertoire et tous ses sous-répertoires et fichiers.    |rm-r LINUX : Proposera un choix pour éviter de supprimer des fichiers sécurisés.  |
| | |
|mkdir   £ md  £ Crée un nouveau répertoire au chemin spécifié. Si les répertoires n’existent pas déjà sur le chemin d’accès, md les crée| mkdir /home/oracle pour créer le répertoire /home/oracle. CMD : on peux également utiliser mkdir en cmd.   |
| | |
|mv   £ Ren/Move  £ "!()" ne pas déplacer le fichier entre parenthèses. Modifie le nom d’un fichier. Le répertoire et le disque ne peuvent pas être modifiés.| mv /home/oracle/1.txt /home pour déplacer le fichier 1.txt dans le répertoire /home. même fonction : "rename" CMD;  exemple linux : renommer un fichier fichier1 en fichier2 : mv fichier1 fichier2 |
| | |
|find /locate  £ dir -s   £ Affiche tous les dossiers et fichiers du répertoire courant.| find /home/oracle -type f -name 1.txt pour chercher le fichier 1.txt dans le répertoire /home/oracle. CMD: Dir-s , Dir -a Dir, -b = Vous pouvez limiter l’édition par attributs (/A), simplifier la liste (/B), ou afficher tous les sous-répertoires et leurs fichiers (/S).|
| | |
|man   £ help   £ Affiche un texte d’aide pour une commande spécifique.   |CMD : "/? "  |
| | |
|chmod    £ icacls   £ modifie les permissions d'un fichier   | (utile plus tard)  chmod 755 /home/oracle/1.txt pour donner tous les droits sauf le droit d'éditer le fichier 1.txt.    |
| | |
|pwd   £ echo %cd%    £ afficher le répertoire de travail courant   | pwd /home/george |
| | |
|cat   £ type   £ affiche le contenu d'un fichier à l'écran   | cat /home/oracle/1.txt pour afficher le contenu de 1.txt.   |
| | |
|echo   £ echo  £ affiche un message   | echo "Je suis en BTS SIO" |
| | |
|whoami   £ Net User  £ affiche le nom d'utilisateur   | whoami fonctionne sous windows  |
| | |
|reset   £ reset   £ Réinitialise une session.   |CMD : rwinsta   |