﻿Alex Lefevre 07/11       
      
# Etape 1      
        
-Virtual box 6.0    
-Windows 10 64 bit    
-1) Redémarrer l'ordinateur       
2) Appuyer de manière répétée sur un bouton spécifique à l'ordinateur (habituellement f2, f11 ou f12)      
3) Trouver "Virtualization Technology", ou similaire, et s'assurer qu'il soit "Enabled"        
-Virtualisation de logiciels:virtualisation de système d'exploitation, d’applications, de services ou de la mémoire.                
               
# Etape 2            
                
-Debian 32 bit version 10.1         
-Etapes d'installation :          
1) Nom de la machine/Dossier dans lequelle elle s'installe/Type de système/Version du système        
2) Mémoire que pourra utiliser la machine virtuelle, 1go de ram de base           
3) Créer un espace disque (avec "créer un disque virtuel maintenant", 8 gio de base) pour éviter les problèmes liés au stockage        
4) Sélectionner "VDMK" pour éviter les problèmes de compatibilité si on l'ouvre avec un autre programme           
5) Sélectionner "Dynamiquement alloué" pour éviter d'occuper inutilement 8go sur le disque dur           
6) Nommer le dossier/fichier contenant la machine et limiter la taille de stockage de la machine          
7) La machine est créée             
8) La démarrer et séléctionnez le .iso correspond au type et version de l'étape 1           
9) La machine fonctionne.           
10) Choisir les mots de passe et noms d'utilisateurs      
-login:alex / cr...           
-Gnome/openssh  (Serveur SSH)        
           
# Etape 3     
           
-wifi itescia         
-Utiliser NetworkManager          
-ping google.com      
-mode promiscuité:tout autoriser        
-dhclient       
      
# Etape 4     
     
-Installer les additions invités -> dossiers partagés dans configuration -> ajouter nouveau dossier -> chemin /home/alex/share -> sudo mount -t vboxsf partage ~/share/         
-fait    
-refaire la ligne de commande (sudo mount ... ) à chaque fois. Sinon cocher "montage automatique"    

# Etape 5    
    
-Configuration -> Réseau -> Accès par pont    
-Ping la machine hote depuis la machine virtuelle et inversement    
Statistiques Ping pour 192.168.1.19:           
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),        
Durée approximative des boucles en millisecondes :       
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms       





