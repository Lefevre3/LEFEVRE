﻿12/09/2019
Robin Teissedre et Alex Lefevre

Activité 1 : Configuration d'un ordinateur

Configuration 2 :
==
Matériel :
-
LENOVO V130-15IKB (81HN00FCFR) : <a href="https://urlz.fr/aCYa">https://urlz.fr/aCYa</a> - *Ordinateur 416 Euros*    
Advance Shape 6D Wired Mouse : <a href="https://urlz.fr/aCY9">https://urlz.fr/aCY9</a> - *Souris 10 Euros*      
<br/>  
**Logiciels :** Microsoft office 365  
**Site d'achat :** <a href="https://urlz.fr/aCY8">https://urlz.fr/aCY8</a>  

**Prix total :** 496 Euros  

Explication : 
==

1. Cet ordinateur dispose d'un écran 15,6 pouces HD (Les écrans 16 pouces n'existent pas) avec un traitement anti-reflets ainsi que d'un clavier ergonomique idéal pour travailler.   
    
2. Il est suffisamment puissant pour toute activité professionnelle. Il y a un disque dur de 1To ce qui permet de stocker plus que nécessaire.  
  
3. L'ordinateur a une autonomie de 6 heures et peut être facilement transporté dû à son faible poids (1,80 kg) et à sa faible épaisseur (2,23 cm).  
  
4. Il a été conçu pour résister aux chocs. Il peut être connecté à Internet par WiFi ou par cable ethernet classique (RJ45).  
  
5. Il y a un port HDMI permettant de connecter l'ordinateur à un vidéoprojecteur.  
  
6. La souris est parfaite pour une utilisation professionnelle.  
  
7. Microsoft Office coûte soit 70 Euros par An, soit 150 Euros pour un achat définitif.  