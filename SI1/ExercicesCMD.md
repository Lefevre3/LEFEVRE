﻿10/10/2019
Alex Lefevre

Exercices CMD

Exercice 1 Windows :

cd C:\Users\User
mkdir ligne-commande
cd ligne-commande
mkdir dossier
mkdir rep1
mkdir rep2
mkdir rep3
echo off > rep1\fichier11.txt
echo off > rep1\fichier12.txt
echo off > rep2\fichier22.txt
mkdir rep2\rep21
mkdir rep3\rep31
mkdir rep3\rep32
echo MS-DOS > rep1\fichier11.txt
echo Windows > rep1\fichier12.txt
echo Linux > rep2\fichier22.txt

Exercice 1 Linux :

cd /c/Users/User/
mkdir ligne-commande
cd ligne-commande
mkdir dossier
mkdir rep1
mkdir rep2
mkdir rep3
echo > rep1/fichier11.txt
echo > rep1/fichier12.txt
echo > rep2/fichier22.txt
mkdir rep2/rep21
mkdir rep3/rep31
mkdir rep3/rep32
echo MS-DOS > rep1/fichier11.txt
echo Windows > rep1/fichier12.txt
echo Linux > rep2/fichier22.txt

Exercice 2 Windows :

m:
dir
mkdir exo2
cd exo2
echo off > moi.txt
echo Je suis en BTS SIO
echo Je suis en BTS SIO > moi.txt
echo Et j’aime ça ! >> moi.txt
m:
set
whoami OU net user

Exercice 2 Linux:

cd //
ls -al  (ou cd + TAB)
mkdir exo2
cd exo2
echo > moi.txt  (on peut aussi utiliser touch)
echo Je suis en BTS SIO
echo Je suis en BTS SIO > moi.txt
echo Et j’aime ça ! >> moi.txt
cd /m/
set
whoami OU net user